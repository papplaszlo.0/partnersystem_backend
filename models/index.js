"use strict";
const Sequelize = require("sequelize");
const env = process.env.DB_ENV || "development";
const config = require("../config/config.json")[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

const models = [
  require("./companyforms")(sequelize, Sequelize),
  require("./locations")(sequelize, Sequelize),
  require("./partners")(sequelize, Sequelize)
];
models.forEach(model => {
  db[model.name] = model;
});

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
