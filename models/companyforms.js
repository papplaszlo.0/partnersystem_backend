'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class companyForms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  companyForms.init({
    abbreviation: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'companyForms',
  });
  return companyForms;
};