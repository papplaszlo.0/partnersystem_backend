"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class partners extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  partners.init(
    {
      companyName: DataTypes.STRING,
      formID: DataTypes.INTEGER,
      taxNumber: DataTypes.STRING,
      registrationNumber: DataTypes.STRING,
      locationID: DataTypes.INTEGER,
      address: DataTypes.STRING,
      phone: DataTypes.STRING,
      bankAccountNum: DataTypes.STRING,
      comment: DataTypes.TEXT
    },
    {
      sequelize,
      modelName: "partners"
    }
  );
  return partners;
};
