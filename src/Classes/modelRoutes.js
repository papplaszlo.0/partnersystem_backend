/** This class generates the default routes for a Sequelize model.
 * Therefore we don't create express router for each application routes and same methods for all model.
 * This class generate the following routes: Get all, get one, create, update and delete record in a model. */

class modelRoutes {
  constructor(model) {
    if (!model) {
      console.log("Error! Sequelize model is not defined.");
      return;
    }

    this.router = require("express").Router();

    // This model will be used, when generating router
    this.model = model;
  }

  // Generate route for find all data from table
  getAllRecord() {
    this.router.get("/", async (req, res) => {
      let result;

      // Find all record
      try {
        result = await this.model.findAll();
      } catch (error) {
        console.log(error);
        res.status(400).send("Hiba! Az adatok listázása sikertelen.");
        return;
      }

      res.send(result);
      return;
    });
  }

  // Generate route for create a new record
  createRecord() {
    this.router.post("/", async (req, res) => {
      let newRecord;

      try {
        newRecord = await this.model.create(req.body);
      } catch (error) {
        console.log(error);
        res.status(400).send("Hiba! Sikertelen adatfelvitel.");
        return;
      }

      res.send(newRecord.dataValues);
      return;
    });
  }

  // Generate route for update a record
  updateRecord() {
    this.router.put("/", async (req, res) => {
      let record;

      // Find the record, which needs to updated
      try {
        record = await this.model.findByPk(req.body.id);
      } catch (error) {
        console.log(error);
        res.status(400).send("Hiba! A módosítás sikertelen.");
        return;
      }

      // If can not find the record, return with error
      if (!record) {
        res.status(400).send("Hiba! A módosítás sikertelen.");
        return;
      }

      // Updating the record
      try {
        await record.update(req.body);
      } catch (error) {
        console.log(error);
        res.status(400).send("Hiba! A módosítás sikertelen.");
        return;
      }

      res.send(record.dataValues);
      return;
    });
  }

  // Generate route for delete record
  deleteRecord() {
    this.router.delete("/:id", async (req, res) => {
      let recordToDelete;

      // Check record is exist what should be deleted
      try {
        recordToDelete = await this.model.findByPk(req.params.id);
      } catch (error) {
        console.log(error);
        res.status(400).send("Hiba! A törlés sikertelen.");
        return;
      }

      // If record can not find, stop deleting and send error
      if (!recordToDelete) {
        res.status(400).send("Hiba! A törlés sikertelen.");
        return;
      }

      // Deleting the record
      try {
        await recordToDelete.destroy();
      } catch (error) {
        console.log(error);
        res.status(400).send("Hiba! A törlés sikertelen.");
        return;
      }

      res.send("DELETED");
      return;
    });
  }

  getRouter() {
    this.getAllRecord();
    this.createRecord();
    this.updateRecord();
    this.deleteRecord();

    return this.router;
  }
}

module.exports = modelRoutes;
