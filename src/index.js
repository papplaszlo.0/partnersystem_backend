const express = require("express");
const app = express();
const cors = require("cors");
const dotenv = require("dotenv").config();
const port = process.env.PORT || 5000;
const models = require("../models/index");
const modelRoutes = require("./Classes/modelRoutes");

app.use(cors());
app.use(express.json());

// Adding routes
app.use("/partners", new modelRoutes(models.partners).getRouter());
app.use("/companyForms", new modelRoutes(models.companyForms).getRouter());
app.use("/locations", new modelRoutes(models.locations).getRouter());

// Start the backend service
app.listen(port, () => {
  console.log(
    "Partner System backend service started. Listening on port: " + port
  );
});
