"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("partners", [
      {
        companyName: "Demo partner 1",
        formID: 1,
        taxNumber: "12345678-9-10",
        registrationNumber: "1111",
        locationID: 1,
        address: "Nincs-ilyen utca 1.",
        phone: "06-40-506070",
        bankAccountNum: "12343546-78910112-00000000",
        comment: "Demo partner 1 comment",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        companyName: "Demo partner 2",
        formID: 2,
        taxNumber: "12345678-9-10",
        registrationNumber: "2222",
        locationID: 2,
        address: "Sehol utca 2.",
        phone: "06123456789",
        bankAccountNum: "12343546-78910112-00000000",
        comment: "Demo partner 2 comment",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        companyName: "Demo partner 3",
        formID: 3,
        taxNumber: "12345678-9-10",
        registrationNumber: "3333",
        locationID: 3,
        address: "Nekeresd utca 3.",
        phone: "06987654321",
        bankAccountNum: "12343546-78910112-00000000",
        comment: "Demo partner 3 comment",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
