"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("companyForms", [
      {
        abbreviation: "Kft.",
        description: "Korlátolt felelősségű társaság",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        abbreviation: "Rt.",
        description: "Részvénytársaság",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        abbreviation: "Bt.",
        description: "Betéti társaság",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
